class RemoveColumnUser < ActiveRecord::Migration[5.2]
  def change
    remove_column :answers, :user
  end
end

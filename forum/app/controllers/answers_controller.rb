class AnswersController < ApplicationController
  def new
    @answer = Answer.new
  end
  
  def create
    @user = current_user
    @question = Question.find(params[:question_id])
    @answer = @question.answers.new(answer_params)
    if @answer.save!
      redirect_to question_path(@question)
    else
      render 'new'
    end
  end

  def destroy
    @question = Question.find(params[:question_id])
    @answer = @question.answers.find(params[:id])
    @answer.destroy
    redirect_to question_path(@question)
  end
    
  private
    def answer_params
      params.require(:answer).permit(:body, @current_user_id)
    end
end

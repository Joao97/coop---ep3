class Question < ApplicationRecord
    has_many :answers, dependent: :destroy
    belongs_to :user
    validates :assunto, presence: true,
    length: { minimum: 2 }
end

class User < ApplicationRecord
  has_many :answers, dependent: :destroy
  accepts_nested_attributes_for :answers
  has_many :questions, dependent: :destroy
  accepts_nested_attributes_for :questions
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

end

class Ability
  include CanCan::Ability

  def initialize(user)
    alias_action :create, :read, :update, :destroy, to: :crud
    if user.present?
      can :read, :all
      can :crud, Question, user_id: user.id
      can :create, Answer, :all
      can :destroy, Answer, user_id: user.id
      #return unless user && user.admin?
       # can :access, :rails_admin 
        #can :read, :dashboard
    end
  end
end
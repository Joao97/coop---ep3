Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  get 'welcome/index'
 
  resources :questions do
    resources :answers
  end
 
  root 'welcome#index'
end